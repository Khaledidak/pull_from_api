import requests
import json
import datetime as dt
from pymongo import MongoClient, database 
import pandas as pd
import certifi





# write to JSON file and properly formats the file
def write_to_file(nameOfFile, data):
    with open(nameOfFile, 'w+') as f:
        print("Dumping JSON to", nameOfFile)
        file = json.dump(data, f, sort_keys=True, indent=4, separators=(',', ':'))
    return file

def get_twits(ticker):
    url = "https://api.stocktwits.com/api/2/streams/symbol/{0}.json".format(ticker)
    response = requests.get(url).json()
    return response


def get_twits_list(ticker):
    ret = {}
    print("Getting data for", ticker)
      
    try:
        print('get_twits_list for the ticker '+ticker)
        data = get_twits(ticker)
        symbol = data['symbol']['symbol']
        msgs = data['messages']
        ret.update({symbol: msgs})
       
    except Exception as e:
        print(e)
        print("Error getting", ticker)
        
    return ret

def build_dataframe(ticker,data):
    Columns = ['id', 'body', 'created_at', 'user_id','like', 'sentiment', 'followers', 'following','Creation_ts']
    rows = []
    for i in range(len(data[ticker])):
        try:
            likes = data[ticker][i]['likes']['total']
        except KeyError:
            likes = 0
       
            
        
        rows.append({'id': data[ticker][i]['id'], 
                    'body': data[ticker][i]['body'], 
                    'created_at': data[ticker][i]['created_at'],
                    'user_id': data[ticker][i]['user']['id'], 
                    'likes': likes,
                    'sentiment': data[ticker][i]['entities']['sentiment'],
                    'followers': data[ticker][i]['user']['followers'],
                    'following': data[ticker][i]['user']['following'],
                    'Creation_ts' : str(dt.datetime.now())
                    })
   
    df=pd.DataFrame(data= rows)  
     
    return df
def connect_to_mongo(MyCollection ):
  
    myclient =MongoClient('mongodb+srv://admin123:admin123@stockmanager.siluq.mongodb.net/test',tlsCAFile=certifi.where())
   
    mydb = myclient["Twitstock_Dataframe"]
    
    if MyCollection in (mydb.list_collection_names()) :
        mycol = mydb[MyCollection] 
        print(mydb.list_collection_names())
    else : 
        mycol = mydb.create_collection(name=(MyCollection))
        try:
            mycol.create_index('id', unique=True)
        except:
            pass
    
    return mycol

def insert_into_mycollection(mycol,df):
    records = json.loads(df.T.to_json()).values()
    try:
        mycol.insert_many(records)
    except:
        pass
  


if __name__ == "__main__":
    
    tickers = ['AAPL','GOOGL','MSFT','AMZN','FB','TSLA','GE','JPM','TSM','BAC','V','MA','PFE','IDXX','JNJ','AZN']
  
    #tickers = ['AAPL','ADI','ASML','BAC','FB','GOOG','GOOGL','JPM','MSFT','NVDA','TSM','WFC','LIN','BHP','XOM','GE']
    for ticker in tickers:
      
        dateTimeObj = dt.datetime.now()
        timestampStr = dateTimeObj.strftime("%d%b%Y%H%M%S")
        FILENAME = str(ticker)+'_'+(timestampStr)+'.json'
        data = get_twits_list(ticker)
        t=str(ticker)
        
        df=build_dataframe(t,data)
       
       
        
        MyCollection = connect_to_mongo(t)
        insert_into_mycollection(MyCollection,df)
    

       
        
        
